<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Live extends Model
{
    protected $table = 'b_live';

    /**
     * 观看人数
     *
     * @return string
     */
    public function viewNum()
    {
        return "1111观看";
    }

    /**
     * 主播信息
     *
     * @return HasOne
     */
    public function liveUser() : HasOne
    {
        return $this->hasOne(User::class,'user_id','live_user_id');
    }

    public function relateStore()
    {


    }

    public function users()
    {
        return $this->hasMany(User::class,'user_id','live_user_id');
    }



}
