## 环境要求
- PHP >= 7.2.5
- Composer 1.10.*
- MySQL 5.6.*

## 首次使用
1.使用composer安装vendor目录
```shell script
composer install
```
2.重写环境变量文件
```shell script
cp .env.example .env
```
3.填充项目所需的数据与服务：Mysql


## 使用GraphQL
默认情况下，GraphQL的接口通过/graphql访问，GraphQL控制台可以通过/graphql-playground访问。
![graphql-playground](graphql-playground.png)

## 总结
1.使用[nuwave/lighthouse](https://github.com/nuwave/lighthouse) composer包搭建GraphQL环境，此包直接将 Schema 映射到模型属性或模型关系上，中间很难插入其他处理逻辑，或特殊定制化的属性。

2.如遇到突发状况，需要临时修改接口返回字段，可能对其他模块接口影响较大。

3.GraphQL返回字段虽然丰富，但所涉及关联查询语句也增加，有可能造成冗余查询，在没有较高性能后端设计，性能恐会有所影响。

4.Laravel框架在HTTP服务方面提供的大部分功能都是基于 RESTFul  而 GraphQL 与 RESTFul 的服务方式是迥异的，GraphQL 是在自己的 Schema 中注册路由，并在请求的查询语句中解析路由，这使得 Laravel 的路由、控制器、请求、响应 和 中间件都无法起完整作用。


## 扩展
1. GraphQL在微服务中的应用？
2. GraphQL与RESTful相互结合？

## 参考
- https://laravel.com/docs/7.x
- https://lighthouse-php.com/
- https://learnku.com/laravel/t/34846
